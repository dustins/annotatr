var del = require('del'),
    gulp = require('gulp'),
    copy = require('gulp-copy'),
    jspm = require('gulp-jspm'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    sequence = require('gulp-sequence'),
    magicImporter = require('node-sass-magic-importer'),
    merge = require('merge-stream'),
    typescript = require('gulp-typescript'),
    watch = require('gulp-watch');

/**
 * Deletes generated files from the public folder
 */
gulp.task('clean', function (done) {
    return del(['src/main/resources/static/**'], {force: true}).then(() => {
        return done;
    });
});

/**
 * Copy JSPM config to public folder
 */
gulp.task('build:jspm:config', function () {
    return gulp.src('src/main/web/js/jspm/config.js')
        .pipe(copy('src/main/resources/static', {prefix: 3}))
});

/**
 * Create a SystemJS bundle of all dependencies using JSPM (app still loaded dynamically)
 */
gulp.task('build:jspm:bundle', function () {
    return gulp.src('src/main/web/js/main.ts')
        .pipe(jspm({
            fileName: 'deps',
            plugin: 'ts',
            minify: true,
            arithmetic: "- [app/**/*] + ts"
        })).pipe(gulp.dest('src/main/resources/static/js/jspm'));
});

/**
 * Copy JSPM libraries (SystemJS) to public folder
 */
gulp.task('build:jspm:libs', function () {
    return gulp.src('src/main/web/js/jspm/packages/*')
        .pipe(copy('src/main/resources/static', {prefix: 3}));
});

gulp.task('build:jspm', sequence(['build:jspm:libs', 'build:jspm:config', 'build:jspm:bundle']));

/**
 * Build a SystemJS bundle of all dependencies using JSPM (including app)
 */
gulp.task('build:jspm:release', function () {
    return gulp.src('src/main/web/js/main.ts')
        .pipe(jspm({
            fileName: 'app',
            plugin: 'ts',
            minify: true,
        })).pipe(gulp.dest('src/main/resources/static/js/jspm'));
});

/**
 * Copy app files and generated javascript files to public folder
 */
gulp.task('build:app', function () {
    var project = typescript.createProject('tsconfig.json', {
        rootDir: "src/main/web/js"
    });
    var result = project.src().pipe(typescript(project));
    var typescriptPipe = result.js.pipe(gulp.dest('src/main/resources/static/js'));

    var appPipe = gulp.src(['src/main/web/js/main.ts', 'src/main/web/js/app/**'], {ignoreInitial: true})
        .pipe(copy('src/main/resources/static/js', {prefix: 4}));

    return merge(appPipe, typescriptPipe);
});

/**
 * Watch for JSPM changes and kick off necessary tasks
 */
gulp.task('watch:jspm', function () {
    gulp.watch(['src/main/web/js/jspm/*'], ['build:jspm:config']);
    return watch(['src/main/web/js/jspm/packages/**'], {ignoreInitial: true}, ['build:jspm:bundle']);
});

/**
 * Watch for app changes and kick off necessary tasks
 */
gulp.task('watch:app', function (done) {
    return gulp.watch(['src/main/web/js/app/**', 'src/main/web/js/main.ts'], {debounceDelay: 500}, ['build:app']);
});

/**
 * Copies compiled sass files to public folder
 */
gulp.task('build:sass', function () {
    return gulp.src(['src/main/web/scss/*.scss'])
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass({
            importer: magicImporter
        }))
        .pipe(gulp.dest('src/main/resources/static/css', {prefix: 4}));
});

/**
 * Watch sass files and recompile if any of them change
 */
gulp.task('watch:sass', function (done) {
    return gulp.watch(['src/main/web/scss/**'], {debounceDelay: 500}, ['build:sass']);
});

/**
 * Perform `clean` and then run all `build:*` tasks
 */
gulp.task('build', sequence('clean', ['build:sass', 'build:jspm', 'build:app']));

/**
 * Run all the watch:* tasks
 */
gulp.task('watch', function (done) {
    gulp.start('watch:sass', 'watch:jspm', 'watch:app')
});

/**
 * Build and watch
 */
gulp.task('default', sequence('build', 'watch'));
