package net.swigg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnotatrApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnnotatrApplication.class, args);
    }
}
