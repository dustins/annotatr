package net.swigg

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
 * @author Dustin Sweigart <dustin@swigg.net>
 */
@Controller
class HomeController {
    @RequestMapping("/")
    fun home(): String {
        return "home/home";
    }

    @RequestMapping("/app")
    fun app(): String {
        return "home/app";
    }
}