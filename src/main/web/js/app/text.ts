import {Token, TokenInterface} from "./token";

export interface TextInterface {
    input: string,

    nodes: Array<TokenInterface>
}

export class Text {
    constructor(public originalText: string, public nodes: Array<Token>) {
    }

    static from(json: TextInterface): Text {
        return new Text(json.input, json.nodes.map(Token.from))
    }
}