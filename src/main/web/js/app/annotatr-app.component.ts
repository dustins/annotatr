import {Text, TextInterface} from "./text";
import * as d3 from "d3/build/d3.node.js";
import "zepto";
import {ElementRef, OnInit, Component} from "@angular/core";
import {sprintf} from "sprintf-js/dist/sprintf.min.js";

declare var $: any;

var someJson: TextInterface = {
    "input": "1 large brown egg",
    "nodes": [{
        "id": 0,
        "form": "@#r$%",
        "annotations": {
            "Lemma": {"@type": "LemmaAnnotation", "value": "@#r$%", "type": "Lemma"},
            "UniversalPartOfSpeech": {
                "@type": "UniversalPartOfSpeechAnnotation",
                "value": "@#r$%",
                "type": "UniversalPartOfSpeech"
            },
            "NamedEntity": {"@type": "NamedEntityAnnotation", "value": "@#r$%", "type": "NamedEntity"},
            "Features": {"@type": "FeaturesAnnotation", "type": "Features", "value": {}}
        }
    }, {
        "id": 1,
        "form": "1",
        "annotations": {
            "Lemma": {"@type": "LemmaAnnotation", "value": "0", "type": "Lemma"},
            "UniversalPartOfSpeech": {
                "@type": "UniversalPartOfSpeechAnnotation",
                "value": "CD",
                "type": "UniversalPartOfSpeech"
            },
            "NamedEntity": {"@type": "NamedEntityAnnotation", "value": "U-CARDINAL", "type": "NamedEntity"},
            "Features": {"@type": "FeaturesAnnotation", "type": "Features", "value": {"pos2": "LS"}},
            "DependencyRelation": {
                "@type": "DependencyRelationAnnotation",
                "value": {"first": 4, "second": "nmod"},
                "type": "DependencyRelation",
                "primary": true
            }
        }
    }, {
        "id": 2,
        "form": "large",
        "annotations": {
            "Lemma": {"@type": "LemmaAnnotation", "value": "large", "type": "Lemma"},
            "UniversalPartOfSpeech": {
                "@type": "UniversalPartOfSpeechAnnotation",
                "value": "JJ",
                "type": "UniversalPartOfSpeech"
            },
            "NamedEntity": {"@type": "NamedEntityAnnotation", "value": "O", "type": "NamedEntity"},
            "Features": {"@type": "FeaturesAnnotation", "type": "Features", "value": {}},
            "DependencyRelation": {
                "@type": "DependencyRelationAnnotation",
                "value": {"first": 4, "second": "nmod"},
                "type": "DependencyRelation",
                "primary": true
            }
        }
    }, {
        "id": 3,
        "form": "brown",
        "annotations": {
            "Lemma": {"@type": "LemmaAnnotation", "value": "brown", "type": "Lemma"},
            "UniversalPartOfSpeech": {
                "@type": "UniversalPartOfSpeechAnnotation",
                "value": "JJ",
                "type": "UniversalPartOfSpeech"
            },
            "NamedEntity": {"@type": "NamedEntityAnnotation", "value": "O", "type": "NamedEntity"},
            "Features": {"@type": "FeaturesAnnotation", "type": "Features", "value": {}},
            "DependencyRelation": {
                "@type": "DependencyRelationAnnotation",
                "value": {"first": 4, "second": "nmod"},
                "type": "DependencyRelation",
                "primary": true
            }
        }
    }, {
        "id": 4,
        "form": "egg",
        "annotations": {
            "Lemma": {"@type": "LemmaAnnotation", "value": "egg", "type": "Lemma"},
            "UniversalPartOfSpeech": {
                "@type": "UniversalPartOfSpeechAnnotation",
                "value": "NN",
                "type": "UniversalPartOfSpeech"
            },
            "NamedEntity": {"@type": "NamedEntityAnnotation", "value": "O", "type": "NamedEntity"},
            "Features": {"@type": "FeaturesAnnotation", "type": "Features", "value": {}},
            "DependencyRelation": {
                "@type": "DependencyRelationAnnotation",
                "value": {"first": 0, "second": "root"},
                "type": "DependencyRelation",
                "primary": true
            }
        }
    }]
};

@Component({
    selector: 'annotatr-app',
    templateUrl: 'js/app/annotatr-app.component.html'
})
export class AnnotatrAppComponent implements OnInit {

    svg: SVGElement;

    text: Text;

    constructor(private element: ElementRef) {
    }

    ngOnInit(): any {
        this.svg = $('svg', this.element.nativeElement).get(0);
        this.text = Text.from(someJson);
        this.layout();
    }

    layout() {
        var svg = this.svg,
            padding = 15,
            spacing = 20,
            duration = 1000;

        var textGroup = d3.select(this.svg).append('g')
            .classed('nodes', true)
            .attr('transform', sprintf("translate(%f,%f)", $(svg).width() / 2, $(svg).height() / 6));

        var node = textGroup.selectAll('text')
            .data(this.text.nodes)
            .enter()
            .append('g')
            .classed("textNode", true);

        node.append('rect');

        node.append('text')
            .text(d => d.form);

        var textNodes = $('text', svg),
            widths = textNodes.map((i, n) => $(n).width() + padding + spacing).toArray(),
            totalWidth = widths.reduce((a, b) => a + b);

        textGroup.selectAll('rect')
            .data(this.text.nodes)
            .attr('width', (d, i) => $(textNodes[i]).width() + padding * .8)
            .attr('height', (d, i) => $(textNodes[i]).height() + padding)
            .attr('rx', 3)
            .attr('transform', (d, i) => sprintf("translate(%f,%f)", padding / -2 + 1.5, -($(textNodes[i]).height() + padding / 2)));

        textGroup.transition()
            .duration(duration)
            .attr('transform', sprintf("translate(%f,%f)", $(svg).width() / 2 - totalWidth / 2, $(svg).height() / 2));

        textGroup.selectAll('g')
            .data(this.text.nodes)
            .transition()
            .duration(duration)
            .attr('transform', (d, i) => sprintf("translate(%f,0)", ([0].concat(widths).slice(0, i + 1).reduce((a, b)=>a + b) + spacing)));
    }
}