import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AnnotatrAppComponent} from "./annotatr-app.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [AnnotatrAppComponent],
    bootstrap: [AnnotatrAppComponent]
})
export class AppModule {
}