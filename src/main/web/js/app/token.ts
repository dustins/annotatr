import {Annotation} from "./annotation";

export interface TokenInterface {
    id: number,

    form: string,

    annotations: Map<String,Annotation>
}

export class Token {
    constructor(public position: number, public form: string, public annotations: Map<string,Annotation>) {
    }

    static from(json: TokenInterface): Token {
        return new Token(json.id, json.form, json.annotations)
    }
}