# annotatr

The goal of annotatr is to make it easy to quickly annotate text for training natural language processors.

## Getting Started

**Steps**
1. `npm install` -- install all the required node modules
2. `jspm install` -- build javascript
3. `gulp build` -- build assets
4. `./gradlew bootRun` -- run with gradle wrapper

**Assumptions**
* **java** is already installed
* **npm** is already installed
   * gulp/jspm are installed, or you prefix the commands with `./node_modules/.bin/`